require "spec_helper"
require "cog_cmd/swat/reload"

describe CogCmd::Swat::Reload do
  before do allow(STDIN).to receive(:tty?) { true } end

  let(:git) { GitSpecHelper.new }

  before do
    ENV["SCRIPTS_REMOTE_URL"] = git.source_repo
    ENV["SCRIPTS_LOCAL_PATH"] = git.target_dir
  end

  it "reloads the repo" do
    command = CogCmd::Swat::Reload.new
    command.run_command
    expect(command.response.content).to include(
      source: git.source_repo,
      target: git.target_dir,
      action: "clone",
      wiped: false
    )
  end

  it "reloads the repo" do
    begin
      command = CogCmd::Swat::Reload.new
      ENV["COG_OPTS"] = "wipe"
      ENV["COG_OPT_WIPE"] = "true"
      command.run_command
      expect(command.response.content).to include(
        source: git.source_repo,
        target: git.target_dir,
        action: "clone",
        wiped: true
      )
    ensure
      ENV.delete("COG_OPTS")
      ENV.delete("COG_OPT_WIPE")
    end
  end

  it "reloads the repo 2 times" do
    command = CogCmd::Swat::Reload.new
    command.run_command
    command.run_command
    command.run_command
    expect(command.response.content).to include(
      source: git.source_repo,
      target: git.target_dir,
      action: "pull",
      wiped: false
    )
  end

  after do
    ENV.delete("SCRIPTS_REMOTE_URL")
    ENV.delete("SCRIPTS_LOCAL_PATH")
    git.destroy
  end
end
