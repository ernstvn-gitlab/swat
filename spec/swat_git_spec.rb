require "spec_helper"
require "swat_git"

describe Swat::Git do
  context "Given a valid URL" do
    before(:all) do
      @git = GitSpecHelper.new
    end

    def add_commit
      Dir.chdir(@git.source_repo) {
        `echo "how you doing?" >> readme.md`
        `git commit -a -m "Another line in"`
      }
    end

    before(:each) do @git.clean_target end

    it "can clone a repo" do
      repo = Swat::Git.new(@git.source_repo, @git.target_dir)
      repo.update
      expect(repo.valid?).to be_truthy
    end

    it "can determine that a repo is invalid when it doesn't exists" do
      expect(Swat::Git.new(@git.source_repo, @git.target_dir).valid?).to be_falsey
    end

    it "can clone and then update a repo" do
      repo = Swat::Git.new(@git.source_repo, @git.target_dir)
      repo.update
      add_commit
      repo.update
      expect(repo.valid?).to be_truthy
    end

    it "can wipe a non existing repo" do
      repo = Swat::Git.new(@git.source_repo, @git.target_dir)
      repo.wipe
      expect(File.exist?(@git.target_dir)).to be_falsey
    end

    it "can clone and then wipe a repo" do
      repo = Swat::Git.new(@git.source_repo, @git.target_dir)
      repo.update
      repo.wipe
      expect(File.exist?(@git.target_dir)).to be_falsey
    end

    it "can clone and then update a repo multiple times" do
      repo = Swat::Git.new(@git.source_repo, @git.target_dir)
      repo.update
      3.times do
        add_commit
        repo.update
      end
      expect(Dir.chdir(@git.target_dir) {
               `git log --oneline | wc -l`.strip
             }).to eq("5")
    end

    after(:all) do
      @git.destroy
    end
  end
end
