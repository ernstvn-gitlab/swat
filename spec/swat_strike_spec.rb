require "spec_helper"
require "cog_cmd/swat/strike"

describe CogCmd::Swat::Strike do
  before do allow(STDIN).to receive(:tty?) { true } end
  before do
    ENV["RAILS_RUNNER_COMMAND"] = "./spec/helpers/rails_stub.rb lib/swat_run.rb"
  end
  after do ENV.delete("RAILS_RUNNER_COMMAND") end

  it "can be called" do
    command = CogCmd::Swat::Strike.new
    with_environment(args: ["test success success"]) do
      command.run_command
    end
    expect(command.response.content)
      .to eq("execution_mode"=>"execute",
             "prepare"=>{"successful"=>true, "output"=>"preparation is fine so far"},
             "pre_check" => {"successful"=>true, "output"=>"all is gut"},
             "execute"=>{"successful"=>true,
                         "output"=>"Context so far is {:prepared=>\"done\", :checks=>\"done\"}"})
  end
end
