require_relative "spec_helper"
require_relative "../scripts/test"

describe Swat::CommandExecution do
  it "fails with an invalid execution mode" do
    expect { Swat::CommandExecution.new(Swat::Command.new([]), "invalid") }
      .to raise_error(/Invalid execution mode 'invalid'/)
  end

  it "fails prepare stage without arguments" do
    executor = Swat::CommandExecution.new(Swat::Command.new([]))
    expect(executor.run).to eq(
      execution_mode: "dryrun",
      prepare: { successful: false, output: "I need at least 1 argument" }
    )
  end

  it "stops execution on pre_checks when constraints are not met" do
    executor = Swat::CommandExecution.new(Swat::Command.new([:failure]))
    expect(executor.run).to eq(
      execution_mode: "dryrun",
      prepare: { successful: true, output: "preparation is fine so far" },
      pre_check: { successful: false, output: "something is not right" }
    )
  end

  it "it only goes as far as pre checks in dry run mode" do
    executor = Swat::CommandExecution.new(Swat::Command.new([:success]))
    expect(executor.run).to eq(
      execution_mode: "dryrun",
      prepare: { successful: true, output: "preparation is fine so far" },
      pre_check: { successful: true, output: "all is gut" }
    )
  end

  it "stops execution on pre_checks when prechecks fail" do
    executor = Swat::CommandExecution.new(Swat::Command.new([:failure]), "execute")
    expect(executor.run).to eq(
      execution_mode: "execute",
      prepare: { successful: true, output: "preparation is fine so far" },
      pre_check: { successful: false, output: "something is not right" }
    )
  end

  it "execution can fail and report it" do
    executor = Swat::CommandExecution.new(Swat::Command.new(%i(success failure)), "execute")
    expect(executor.run).to eq(
      execution_mode: "execute",
      prepare: { successful: true, output: "preparation is fine so far" },
      pre_check: { successful: true, output: "all is gut" },
      execute: { successful: false, output: "execution failed!" }
    )
  end

  it "execution can succeed" do
    executor = Swat::CommandExecution.new(Swat::Command.new(%i(success success)), "execute")
    expect(executor.run).to eq(
      execution_mode: "execute",
      prepare: { successful: true, output: "preparation is fine so far" },
      pre_check: { successful: true, output: "all is gut" },
      execute: { successful: true, output: "Context so far is {:prepared=>\"done\", :checks=>\"done\"}" }
    )
  end
end
