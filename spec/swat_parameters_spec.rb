require_relative "spec_helper"

describe Swat::Parameters do
  it "loads the command with arguments" do
    allow(ARGV).to receive(:clone) { %w(dryrun command arg) }
    params = Swat::Parameters.new
    expect(params.execution_mode).to eq("dryrun")
    expect(params.command).to eq("command")
    expect(params.args).to eq(["arg"])
    expect(params.to_s).to eq("dryrun command [\"arg\"]")
  end

  it "fails to load if there is no command" do
    allow(ARGV).to receive(:clone) { [] }
    expect { Swat::Parameters.new }.to raise_error(/Execution mode is mandatory/)
  end
end
