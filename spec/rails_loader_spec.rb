require_relative "spec_helper"
require "rails_loader"

describe Swat::RailsLoader do
  it "can load and execute a command" do
    expect(::Swat::RailsLoader
      .new("./spec/helpers/rails_stub.rb lib/swat_run.rb")
      .run("dryrun test success"))
      .to eq("execution_mode"=>"dryrun",
             "prepare"=>{"successful"=>true, "output"=>"preparation is fine so far"},
             "pre_check"=>{"successful"=>true, "output"=>"all is gut"})
  end

  it "can err out when the return code is not 0" do
    expect {
      ::Swat::RailsLoader
        .new("./spec/helpers/fail_stub.sh")
        .run("dryrun")
    }.to raise_error(/Command dryrun failed with err/)
  end
end
