require_relative "spec_helper"

describe Swat::BaseCommand do
  it "errs when calling prechecks" do
    cmd = Swat::BaseCommand.new(:sentinel)
    expect { cmd.pre_check({}) }.to raise_error(/PreChecks are not defined/)
  end

  it "errs when calling execute" do
    cmd = Swat::BaseCommand.new(:sentinel)
    expect { cmd.execute({}) }.to raise_error(/Execution is not defined/)
  end
end
