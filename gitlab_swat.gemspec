Gem::Specification.new do |s|
  s.name          = "gitlab-swat"
  s.version       = "0.1.3"
  s.date          = "2017-04-01"
  s.summary       = "ChatOps Cog Bundle that enables admins to remotely run predefined scripts in a rails console"
  s.description   = <<~eos
    A Successful Deployment Ends Peacefully With No Bullets Fired.
    If That’s Simply Not Possible, SWAT Uses Special Weapons and Tactics to Keep the Public Safe\n\n
    GitLab-Swat allows admins to quickly deploy scripts that can be remotely executed through a rails console\n
    Enabling fast action by using an external git repository as the scripts source, but keeping safety high by
    enforcing a prepare-pre check-execute model that allows execution break at any stage if things are not going
    as expected
eos
  s.authors       = ["Pablo Carranza"]
  s.homepage      = "https://gitlab.com/gitlab-cog/swat"
  s.files         = `git ls-files -z`.split("\x0")
  s.test_files    = s.files.grep(%r{^(spec)/})
  s.license       = "MIT"

  s.add_dependency "cog-rb", "~> 0.4"

  s.add_development_dependency "rspec", "~> 3.5"
  s.add_development_dependency "rubocop", "~> 0.42"
  s.add_development_dependency "simplecov", "~> 0.13"
end
